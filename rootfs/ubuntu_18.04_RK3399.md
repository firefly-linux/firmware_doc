[TOC]


# Firmware update log


## 2021-10-28
**ubuntu_18.04_RK3399_ext4_v2.10-55-gdb3f844_20211027-1728_DESKTOP.img**

* Update content:
1. mv /system/etc/firmware/[wifi_firmware] to /vendor/etc/firmware
1. firmware: upgrade ap6256 firmware
1. change touchscreen driver from evdev to libinput
1. rt5640: Repair recording function
1. update glmark2 icon
1. add glmark2-es-wayland when wayland enabled
1. relink mali so when display server switch
1. update glmark2 icon path
1. set LC_ALL env from mk-rootfs-arm.sh
1. add minor_update support
1. support set display server
1. update gstreamer env
1. add weston desktop. improve overlay patch policy
1. Insert firefly repo in front of upstream repo
1. fix mali deb error
1. set high priority for firefly repo
1. use mkfs.ext4 -d instand of rsync
1. add edit xml value funciton, no use
1. set high priority for firefly repo
1. set default config by update deb

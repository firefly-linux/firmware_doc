[TOC]


# Firmware update log


## 2021-10-28
**AIO-RK3399PROC-UBUNTU-18.04_DESKTOP-GPT-20211028-1130.img**
**AIO-RK3399PROC-LVDS-UBUNTU-18.04_DESKTOP-GPT-20211028-1131.img**

* KERNEL COMMIT： e626d9dba906513c682f1f10347a56662e22da9b
* FS: ubuntu_18.04_RK3399_ext4_v2.10-55-gdb3f844_20211027-1728_DESKTOP.img

* kernel:
```
e626d9dba906  - zouxf    2021-03-23 : fix overlay case tar x fail
b41e2cf1325f  - huangjc  2021-10-14 : Revert "ovl: modify ovl_permission() to do checks on two inodes"
a69fce215ce4  - huangjc  2021-10-14 : Revert "ANDROID: overlayfs: Fix a regression in commit b24be4acd"
dac808b50809  - huangjc  2021-09-08 : rk3399pro-firefly-aioc-lvds.dts: adapt to gsl touch screen
7f224e911d16  - huangjc  2021-07-08 : scripts: pack all headfile when make deb-pkg/bindeb-pkg, and exclude directory name
6dd76088dc97  - huangjc  2021-05-25 : firefly: firefly3399pro_linux_defconfig: disable kernel version auto set
4697c4dba640  - huangjc  2020-11-25 : firefly:firefly3399pro_linux_defconfig && rk3399pro_npu_pcie_defconfig: remove CONFIG_MALI_MIDGARD_FOR_LINUX
c41d239ab402  - zouxf    2020-09-22 : Merge branch 'rockchip_release' into firefly
19c746014161  - liulq    2020-09-05 : firefly: add overlay rootfs option
2d56481a9022  - Algea .. 2020-07-13 : drm/rockchip: Fixes set HDMI property cause crash
40d52727d411  - Sandy .. 2020-06-18 : drm/rockchip: dw_hdmi: add support legacy api to set property
164702614d76  - Sandy .. 2020-06-18 : drm/rockchip: dsi: add support legacy api to set property
e6245cac1980  - Sandy .. 2020-06-18 : drm/bridge: analogix_dp: add support legacy api to set property
4863afd09d8d  - Sandy .. 2020-06-18 : drm/rockchip: lvds: add support legacy api to set property
aef1a9e603c0  - Sandy .. 2020-06-18 : drm/rockchip: rgb: add support legacy api to set property
b50667d7693a  - Caesar.. 2020-06-19 : arm64/configs: support DMA_CMA config for rockchip_linux_defconfig
```

**FS update content:**
* [ubuntu_18.04_RK3399.md](https://gitlab.com/firefly-linux/firmware_doc/-/blob/master/rootfs/ubuntu_18.04_RK3399.md)


## 2021-1-8
**AIO-RK3399PROC-UBUNTU-MINIMAL-PYTHON3.5-RKNN-OPENCV-20210108-1500.img.7z**

* NAME： AIO-RK3399PROC-UBUNTU-MINIMAL-PYTHON3.5-RKNN-OPENCV-20210108-1500.img.7z
* KERNEL COMMIT： c29a5d0ea411a8689febcfb055ba0b341d03d59a
* MD5： 91a653f3e857672c9f3825574fe77be8
* FS: Firefly_Ubuntu_18.04.5_LTS_MINIMAL_ext4_202012301242_PYTHON3.5-RKNN-OPENCV.img

* kernel:
1. update

* FS:
1. Install PYTHON3.5 RKNN OPENCV


## 2021-1-8
**AIO-RK3399PROC-UBUNTU-PYTHON3.5-RKNN-OPENCV-20210108-1442.img.7z**

* NAME： AIO-RK3399PROC-UBUNTU-PYTHON3.5-RKNN-OPENCV-20210108-1442.img.7z
* KERNEL COMMIT： c29a5d0ea411a8689febcfb055ba0b341d03d59a
* MD5： bfa832fd984da3afe4caabb8be6b8433
* FS: Firefly_Ubuntu_18.04.5_LTS_DESKTOP_ext4_202012301242_PYTHON3.5-RKNN-OPENCV.img

* kernel:
1. update

* FS:
1. Install PYTHON3.5 RKNN OPENCV

## 2020-9-3
**AIO-RK3399PROC-UBUNTU18.04-MINIMAL-GPT-20200903-1549.img.7z**

* NAME： AIO-RK3399PROC-UBUNTU18.04-MINIMAL-GPT-20200903-1549.img.7z
* COMMIT： f54cc370e4314de048aa5adaad1fe1f4144d88bf
* MD5： 4a8de115d9219503f31507c5e2d0853d
* FS: use ubuntu_18.04_arm64_ext4_v2.03-10-g4c3ee72_20200717-1422_MINIMAL.img

**Update content:**
* rootfs:
1. use ubuntu_18.04_arm64_ext4_v2.03-10-g4c3ee72_20200717-1422_MINIMAL.img

## 2020-05-25

**AIO-RK3399PROC-UBUNTU18.04-GPT-20200525-1016.img.7z**

* NAME： AIO-RK3399PROC-UBUNTU18.04-GPT-20200525-1016.img.7z
* COMMIT： f54cc370e431
* MD5： 54ce92c5e6c70da02465664417f36c32
* FS: ubuntu_18.04_arm64_ext4_v2.02-31-g40127b8_20200512-1048_DESKTOP.img +

**Update content:**
* kernel:
1. rk3399pro-firefly-aiojd4.dts: delete-property for OV13850_0
2. firefly: remove isp video device when no mipi camera connect

## 2020-05-16
**AIO-RK3399PROC-UBUNTU18.04-GPT-20200518-1406.img.7z**

* NAME： AIO-RK3399PROC-UBUNTU18.04-GPT-20200518-1406.img.7z
* COMMIT： c37adabdd7124ea7c8f84c74f0c7ac76bd52ce5a
* MD5： 210a5e5ed0e626e11790e6c34ed6b7a4
* FS: ubuntu_18.04_arm64_ext4_v2.02-31-g40127b8_20200512-1048_DESKTOP.img +

**AIO-RK3399PROC-LVDS-UBUNTU18.04-GPT-20200518-1439.img.7z**

* NAME： AIO-RK3399PROC-LVDS-UBUNTU18.04-GPT-20200518-1439.img.7z
* COMMIT： c37adabdd7124ea7c8f84c74f0c7ac76bd52ce5a
* MD5： c14cfdc0733a5589cf7263e5f5446866
* FS: ubuntu_18.04_arm64_ext4_v2.02-31-g40127b8_20200512-1048_DESKTOP.img +

**Update content:**
* kernel:
1. fix enable TXREQUESTCLKHS before dw mipi dsi transfer
2. enabled uboot logo for rk3399pro lvds
3. arm64: dts: Enable typec0 vbus

* FS:
1. Install libopencv-contrib-dev
2. Install firefly-multi-rtsp
3. Install firefly-3399pronpu-driver
4. Fix mipi camera preview issue
5. System startup related issues
